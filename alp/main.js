var http = require("http");
var qs = require("querystring")
var fs = require("fs")
var msgs = []
var last_msg = 0;
var limit = 5;
var server = http.createServer(function (req, res) {




    if (req.method == "GET") {
        fs.exists("public" + req.url, (e) => {
            if (e) {
                fs.readFile("public" + req.url, function (error, data) {

                    if (req.url.split('.')[1] == 'css') {
                        res.writeHead(200, { 'Content-Type': 'text/css' });
                    } else {
                        res.writeHead(200, { 'Content-Type': 'text/html' });
                    }

                    res.write(data);
                    res.end();
                })
            } else {
                fs.readFile("public/404.html", function (error, data) {
                    res.writeHead(404, { 'Content-Type': 'text/html' });
                    res.write(data);
                    res.end();
                })
            }

        })
    } else {
        if (req.url == '/send') {
            var allData = ''
            req.on("data", function (data) {
                console.log(data)
                allData += data;
            })
            req.on("end", function (data) {
                var finish = qs.parse(allData)

                var date = new Date();
                var h=date.getHours()
                var m=date.getMinutes()
                if(date.getHours()<10){
                    h='0'+h
                }
                if(date.getMinutes()<10){
                    m='0'+m
                }
                finish.time = h + ":" + m
                console.log(finish)
                if (msgs.length > limit) {
                    msgs.reverse();
                    msgs.pop();
                    msgs.reverse();
                }
                msgs.push(finish)

                console.log(msgs)
                last_msg++;
                res.writeHead(200, { 'Content-Type': 'text/plain' })
                res.write('')
                res.end();
            })
        }
        if (req.url == '/update') {
            req.on("data", function (data) {

                allData += data;
            })
            req.on("end", function (data) {
                var finish = qs.parse(allData)
                var date1 = Date.now();
                setTimeout(checkupdate, 100, finish, date1, res)
            })
        }

    }




})
function checkupdate(finish, date1, res) {

    if (Date.now() - date1 <= 5000) {
        if (finish.undefinedlast_msg < last_msg) {
            // if (last_msg > limit) {
            //     finish.undefinedlast_msg=limit
            //     last_msg = limit-1
            // }
            var r = last_msg - finish.undefinedlast_msg
            var databack = [];
            console.log(msgs.length);
            for (var i = 1; i < r + 1; i++) {
                console.log(msgs.length - (r - i) - 1, msgs[msgs.length - (r - i) - 1])
                if (msgs[msgs.length - (r - i) - 1] != undefined) {
                    databack.push(msgs[msgs.length - (r - i) - 1])
                }
            }
            res.writeHead(200, { 'Content-Type': 'application/json' })
            res.write(JSON.stringify({ msgs: databack, last_msg: last_msg }))
            res.end();

        } else {
            setTimeout(checkupdate, 100, finish, date1, res)
        }
    } else {
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.write('')
        res.end();
    }
}

server.listen(3000, function () {
    console.log("serwer startuje na porcie 3000")
});
