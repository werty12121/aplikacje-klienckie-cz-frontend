import {Position2D} from './helpers'
import {Support} from './helpers'
import {Ship} from './ship'

export class Board{
    private size:Position2D
    public board
    constructor(size){
        this.size=size
        this.board=[]
        for(var i=0;i<this.size.y;i++){
            this.board[i]=Array()
            for(var j=0;j<this.size.x;j++){
                this.board[i][j]=0
            }
        }
    }
    checkNeighbourhood(pos:Position2D):boolean{
        var x=pos.x 
        var y=pos.y
        var a0=(this.board[y][x]==0)
        var a1=(x+1>=this.board[y].length||this.board[y][x+1]==0)
        var a2=(x-1<0||this.board[y][x-1]==0)
        var a3=(y+1>=this.board.length||this.board[y+1][x]==0)
        var a4=(y-1<0||this.board[y-1][x]==0)
        var a5=(x+1>=this.board[y].length||y+1>=this.board.length||this.board[y+1][x+1]==0)
        var a6=(x-1<0||y+1>=this.board.length||this.board[y+1][x-1]==0)
        var a7=(x-1<0||y-1<0||this.board[y-1][x-1]==0)
        var a8=(x+1>=this.board[y].length||y-1<0||this.board[y-1][x+1]==0)
        if(!(a0&&a1&&a2&&a3&&a4&&a5&&a6&&a7&&a8)){
            return false
        }else{
            return true
        }
    }
    fillWithRandomShips(ships:Array<number>):Array<Ship>{
        var ships_array=[]
        for(var i=0;i<ships.length;i+=0){
            
            var x=Support.randomFromIntRange(0,this.size.x-ships[i])
            var y=Support.randomFromIntRange(0,this.size.x-ships[i])
            var pozOrPion=Support.randomFromIntRange(0,1)
            console.log("pozorpion "+pozOrPion)
            
                var temporaryBool=true
                for(var c=0;c<ships[i];c++){
                    if(temporaryBool){
                        if(pozOrPion==0){
                            temporaryBool=this.checkNeighbourhood(new Position2D(x+c,y))
                        }else{
                            temporaryBool=this.checkNeighbourhood(new Position2D(x,y+c))
                        }
                    }
                }
                console.log( "init :"+temporaryBool+" "+pozOrPion)
                if(temporaryBool==true){
                    console.log("true"+ships[i])
                    let tempShip=new Ship(ships[i], new Position2D(x,y))
                    ships_array[ships_array.length]=tempShip
                    for(let c=0;c<ships[i];c++){
                        if(pozOrPion==0){
                            this.board[y][x+c]={ship_id:tempShip._id,pos_from_begin:c}
                        }else{
                            this.board[y+c][x]={ship_id:tempShip._id,pos_from_begin:c}
                        }
                        
                    }
                    i++
                }
        }
        return ships_array
    }

}