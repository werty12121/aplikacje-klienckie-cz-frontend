"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ship_1 = require("./ship");
var helpers_1 = require("./helpers");
var decorators_1 = require("./decorators");
var board_1 = require("./board");
var Game = /** @class */ (function () {
    function Game(boardSize) {
        this.shipsToInit = [4, 3, 3, 2, 2, 2, 1, 1, 1, 1];
        this.boardSize = boardSize;
        this.AiShips = [];
        this.humanShips = [];
        this.selected = null;
        this.state = GameState.GameInit;
        this.board = new board_1.Board(this.boardSize);
        this.humanBoard = new board_1.Board(this.boardSize);
        this.state = GameState.AiInit;
        this.initAiShips();
    }
    Game.prototype.getstate = function () {
        return this.state;
    };
    Game.prototype.letHumanBeginInitShips = function () {
        this.drawHumanInitInterface();
    };
    Game.prototype.initAiShips = function () {
        this.AiShips = this.board.fillWithRandomShips(this.shipsToInit);
        this.letHumanBeginInitShips();
        this.state = GameState.HumanInit;
    };
    Game.prototype.drawHumanInitInterface = function () {
        var _this = this;
        var that = this;
        window.oncontextmenu = function () {
            if (that.selected != null) {
                that.selected.mode = !that.selected.mode;
            }
            var arr = document.getElementsByClassName('hover_red');
            for (var h = 0; h < arr.length; h++) {
                arr[h].className = "";
            }
            var arr = document.getElementsByClassName('hover_green');
            for (var h = 0; h < arr.length; h++) {
                arr[h].className = "";
            }
            return false; // cancel default menu
        };
        console.log("drawHumanInitInterface");
        this.clearInterface();
        var mainDiv = document.getElementById("main");
        var shipTable = document.createElement("Table");
        for (var i = 0; i < this.shipsToInit.length; i++) {
            var tr = document.createElement("Tr");
            for (var t = 0; t < this.shipsToInit[i]; t++) {
                var td = document.createElement("Td");
                tr.setAttribute('posY', i.toString());
                td.setAttribute('posY', i.toString());
                td.setAttribute('lengthOfShip', this.shipsToInit[i]);
                td.innerHTML = this.shipsToInit[i];
                tr.appendChild(td);
            }
            tr.onclick = function (e) {
                var tlength, tpos;
                var target = e.target;
                console.log(target.tagName);
                if (target.tagName != "TR") {
                    tlength = target.getAttribute('lengthOfShip');
                    target = target.parentElement;
                }
                else {
                    tlength = target.children[0].getAttribute('lengthOfShip');
                }
                tpos = target.getAttribute('posy');
                console.log(tpos);
                if (target.className == "selected") {
                    var arr = document.getElementsByClassName('selected');
                    for (var h = 0; h < arr.length; h++) {
                        arr[h].className = "";
                    }
                    target.className = "";
                    _this.selected = null;
                }
                else {
                    var arr = document.getElementsByClassName('selected');
                    for (var h = 0; h < arr.length; h++) {
                        arr[h].className = "";
                    }
                    target.className = "selected";
                    _this.selected = { l: tlength, mode: true, pos: tpos };
                    console.log(_this.selected);
                }
            };
            tr.onmouseleave = function (e) {
                var target = e.target;
                if (target.className != "selected")
                    target.className = "";
            };
            tr.onmouseenter = function (e) {
                var target = e.target;
                if (target.className != "selected")
                    target.className = "hover";
            };
            shipTable.appendChild(tr);
        }
        mainDiv.appendChild(shipTable);
        var table = document.createElement("Table");
        table.setAttribute("id", "planszadowyboru");
        for (var i_1 = 0; i_1 < this.boardSize.y; i_1++) {
            var tr = document.createElement("Tr");
            for (var j = 0; j < this.boardSize.x; j++) {
                var td = document.createElement("Td");
                td.setAttribute('posX', j.toString());
                td.setAttribute('posY', i_1.toString());
                td.setAttribute('id', i_1.toString() + '_' + j.toString());
                td.innerHTML = "0";
                td.onmouseenter = function (e) {
                    var target = e.target;
                    //console.log(this.selected)
                    if (_this.selected != null) {
                        var px = parseInt(target.getAttribute('posx'));
                        var py = parseInt(target.getAttribute('posy'));
                        var temp = true;
                        for (var i = 0; i < parseInt(_this.selected.l); i++) {
                            if (_this.selected.mode) {
                                if (temp) {
                                    if (px + parseInt(_this.selected.l) > _this.humanBoard.board[0].length) {
                                        px = _this.humanBoard.board[0].length - parseInt(_this.selected.l);
                                    }
                                    temp = _this.humanBoard.checkNeighbourhood(new helpers_1.Position2D(px + i, py));
                                }
                            }
                            else {
                                if (temp) {
                                    if (py + parseInt(_this.selected.l) > _this.humanBoard.board.length) {
                                        py = _this.humanBoard.board.length - parseInt(_this.selected.l);
                                    }
                                    temp = _this.humanBoard.checkNeighbourhood(new helpers_1.Position2D(px, py + i));
                                }
                            }
                        }
                        if (temp) {
                            podk = "hover_green";
                        }
                        else {
                            podk = "hover_red";
                        }
                        for (var i = 0; i < _this.selected.l; i++) {
                            var px = parseInt(target.getAttribute('posx'));
                            var py = parseInt(target.getAttribute('posy'));
                            var s;
                            var podk;
                            if (_this.selected.mode) {
                                if (px + parseInt(_this.selected.l) > _this.humanBoard.board[0].length) {
                                    px = _this.humanBoard.board[0].length - parseInt(_this.selected.l);
                                }
                                s = document.getElementById(py + "_" + ((px) + i));
                            }
                            else {
                                if (py + parseInt(_this.selected.l) > _this.humanBoard.board.length) {
                                    py = _this.humanBoard.board.length - parseInt(_this.selected.l);
                                }
                                s = document.getElementById(((py) + i) + "_" + (px));
                            }
                            //console.log((py)+"_"+((px)+i))
                            if (s.className != 'ship')
                                s.className = podk;
                        }
                    }
                };
                td.onclick = function (e) {
                    var sc = false;
                    var target = e.target;
                    if (_this.selected != null && target.className != 'ship') {
                        var gpx = parseInt(target.getAttribute('posx'));
                        var gpy = parseInt(target.getAttribute('posy'));
                        var sh = new ship_1.Ship(_this.selected.l, new helpers_1.Position2D(gpx, gpy));
                        for (var i = 0; i < _this.selected.l; i++) {
                            var px = parseInt(target.getAttribute('posx'));
                            var py = parseInt(target.getAttribute('posy'));
                            var s;
                            if (_this.selected.mode) {
                                if (px + parseInt(_this.selected.l) > _this.humanBoard.board[0].length) {
                                    px = _this.humanBoard.board[0].length - parseInt(_this.selected.l);
                                }
                                s = document.getElementById((py) + "_" + ((px) + i));
                            }
                            else {
                                if (py + parseInt(_this.selected.l) > _this.humanBoard.board.length) {
                                    py = _this.humanBoard.board.length - parseInt(_this.selected.l);
                                }
                                s = document.getElementById(((py) + i) + "_" + (px));
                            }
                            console.log(sh._id);
                            console.log(_this.humanBoard.board);
                            if (target.className != 'hover_red') {
                                s.className = "ship";
                                if (_this.selected.mode) {
                                    _this.humanBoard.board[py][px + i] = { ship_id: sh._id, pos_from_begin: i };
                                }
                                else {
                                    _this.humanBoard.board[py + i][px] = { ship_id: sh._id, pos_from_begin: i };
                                }
                                sc = true;
                            }
                        }
                        if (sc) {
                            _this.humanShips.push(sh);
                            var a = document.getElementsByTagName('td');
                            console.log("length: " + a.length);
                            var f = true;
                            for (var i = 0; i < a.length; i++) {
                                if (f)
                                    if (a[i].getAttribute('posy') == _this.selected.pos && a[i].getAttribute('id') == null) {
                                        a[i].parentElement.remove();
                                        f = false;
                                    }
                            }
                            _this.selected = null;
                        }
                        if (_this.humanShips.length == _this.shipsToInit.length) {
                            _this.state = GameState.HumanRound;
                            _this.drawGameInterface();
                        }
                    }
                };
                td.onmouseleave = function (e) {
                    var target = e.target;
                    if (_this.selected != null) {
                        for (var i = 0; i < _this.selected.l; i++) {
                            var px = parseInt(target.getAttribute('posx'));
                            var py = parseInt(target.getAttribute('posy'));
                            var s;
                            if (_this.selected.mode) {
                                if (px + parseInt(_this.selected.l) > _this.humanBoard.board[0].length) {
                                    px = _this.humanBoard.board[0].length - parseInt(_this.selected.l);
                                }
                                s = document.getElementById((py) + "_" + ((px) + i));
                            }
                            else {
                                if (py + parseInt(_this.selected.l) > _this.humanBoard.board.length) {
                                    py = _this.humanBoard.board.length - parseInt(_this.selected.l);
                                }
                                s = document.getElementById(((py) + i) + "_" + (px));
                            }
                            console.log((py) + "_" + ((px) + i));
                            if (s.className != 'ship')
                                s.className = "";
                        }
                    }
                };
                tr.appendChild(td);
            }
            table.appendChild(tr);
        }
        mainDiv.appendChild(table);
    };
    Game.prototype.clearInterface = function () {
        var mainDiv = document.getElementById("main");
        mainDiv.innerHTML = "";
    };
    Game.prototype.drawGameInterface = function () {
        var _this = this;
        this.checkwinner();
        var mainDiv = document.getElementById("main");
        this.clearInterface();
        window.oncontextmenu = function () {
            return true;
        };
        //HUMAN BOARD
        var table = document.createElement("Table");
        table.setAttribute("id", "humanBoard");
        for (var i = 0; i < this.humanBoard.board.length; i++) {
            var tr = document.createElement("Tr");
            for (var j = 0; j < this.humanBoard.board[i].length; j++) {
                var td = document.createElement("Td");
                td.setAttribute('posX', j.toString());
                td.setAttribute('posY', i.toString());
                td.setAttribute('id', i.toString() + '_' + j.toString());
                if (this.humanBoard.board[i][j] == helpers_1.BoardState.HIT) {
                    td.className = 'hover_gray';
                    td.innerHTML = "0";
                }
                else if (this.humanBoard.board[i][j] != 0) {
                    var temp_ship = Game.getShipFromArrayById(this.humanShips, this.humanBoard.board[i][j].ship_id);
                    switch (temp_ship.ship[this.humanBoard.board[i][j].pos_from_begin].state) {
                        case ship_1.ShipState.Normal: {
                            td.className = 'hover_green';
                            break;
                        }
                        case ship_1.ShipState.Hit: {
                            td.className = 'hover_orange';
                            break;
                        }
                        case ship_1.ShipState.Dead: {
                            td.className = 'hover_red';
                            break;
                        }
                    }
                    td.innerHTML = '0'; //(temp_ship._id).toString()
                }
                else {
                    td.innerHTML = "0";
                }
                tr.appendChild(td);
            }
            table.appendChild(tr);
        }
        mainDiv.appendChild(table);
        //AI BOARD
        var table = document.createElement("Table");
        table.setAttribute("id", "aiBoard");
        for (var i = 0; i < this.board.board.length; i++) {
            var tr = document.createElement("Tr");
            for (var j = 0; j < this.board.board[i].length; j++) {
                var td = document.createElement("Td");
                td.setAttribute('posX', j.toString());
                td.setAttribute('posY', i.toString());
                td.setAttribute('id', 'ai_' + i.toString() + '_' + j.toString());
                //console.log(this.board[i][j])
                switch (this.board.board[i][j]) {
                    case helpers_1.BoardState.HIT: {
                        td.className = "hover_gray";
                        break;
                    }
                    case 0: {
                        break;
                    }
                    default: {
                        var tempship = Game.getShipFromArrayById(this.AiShips, this.board.board[i][j].ship_id);
                        //console.log(tempship.ship[this.board[i][j].pos_from_begin])
                        switch (tempship.ship[this.board.board[i][j].pos_from_begin].state) {
                            case ship_1.ShipState.Normal: {
                                console.log("Normal");
                                break;
                            }
                            case ship_1.ShipState.Hit: {
                                console.log("hit");
                                td.className = "hover_orange";
                                break;
                            }
                            case ship_1.ShipState.Dead: {
                                console.log("dead");
                                td.className = "hover_red";
                                break;
                            }
                        }
                        break;
                    }
                }
                if (this.state == GameState.HumanRound) {
                    // td.onmouseenter=(e)=>{
                    //     var target=e.target as HTMLElement 
                    //     console.log("ai"+target.id)
                    //     var arr=document.getElementsByClassName('hover_khaki')
                    //     for(var h=0;h<arr.length;h++){
                    //         arr[h].className=""
                    //         var temp_pos_x=arr[h].id.split('_')[2]
                    //         var temp_pos_y=arr[h].id.split('_')[1]
                    //     }
                    //     target.className='hover_khaki'
                    // }
                    td.onclick = function (e) {
                        var target = e.target;
                        var temp_pos_x = target.id.split('_')[2];
                        var temp_pos_y = target.id.split('_')[1];
                        switch (_this.board.board[temp_pos_y][temp_pos_x]) {
                            case 0: {
                                _this.board.board[temp_pos_y][temp_pos_x] = helpers_1.BoardState.HIT;
                                _this.state = GameState.AiRound;
                                break;
                            }
                            case helpers_1.BoardState.HIT: {
                                return;
                                break;
                            }
                            default: {
                                var temp_ship_2 = Game.getShipFromArrayById(_this.AiShips, _this.board.board[temp_pos_y][temp_pos_x].ship_id);
                                if (temp_ship_2.ship[_this.board.board[temp_pos_y][temp_pos_x].pos_from_begin].state == ship_1.ShipState.Normal) {
                                    console.log(_this.board.board[temp_pos_y][temp_pos_x].pos_from_begin);
                                    temp_ship_2.hit(_this.board.board[temp_pos_y][temp_pos_x].pos_from_begin);
                                    console.log(temp_ship_2.ship);
                                    _this.state = GameState.AiRound;
                                }
                                break;
                            }
                        }
                        var that = _this;
                        setTimeout(function () {
                            that.drawGameInterface();
                        }, 10);
                    };
                }
                tr.appendChild(td);
            }
            table.appendChild(tr);
        }
        mainDiv.appendChild(table);
        var that = this;
        setTimeout(function () {
            if (that.state == GameState.AiRound) {
                var shooted = false;
                while (!shooted) {
                    var x = helpers_1.Support.randomFromIntRange(0, that.boardSize.x - 1);
                    var y = helpers_1.Support.randomFromIntRange(0, that.boardSize.y - 1);
                    switch (that.humanBoard.board[y][x]) {
                        case 0: {
                            that.humanBoard.board[y][x] = helpers_1.BoardState.HIT;
                            shooted = true;
                            break;
                        }
                        case helpers_1.BoardState.HIT: {
                            break;
                        }
                        default: {
                            var temp_ship = Game.getShipFromArrayById(that.humanShips, that.humanBoard.board[y][x].ship_id);
                            if (temp_ship.ship[that.humanBoard.board[y][x].pos_from_begin].state == ship_1.ShipState.Normal) {
                                temp_ship.hit(that.humanBoard.board[y][x].pos_from_begin);
                                shooted = true;
                            }
                            break;
                        }
                    }
                }
                that.state = GameState.HumanRound;
                that.drawGameInterface();
            }
        }, 1000);
    };
    Game.getShipFromArrayById = function (array, id) {
        for (var i = 0; i < array.length; i++) {
            if (array[i]._id == id) {
                return array[i];
            }
        }
        return null;
    };
    Game.prototype.checkwinner = function () {
        var alldead = 0;
        for (var i = 0; i < this.humanShips.length; i++) {
            if (this.humanShips[i].state == ship_1.ShipState.Dead) {
                alldead++;
            }
        }
        if (alldead >= this.humanShips.length) {
            alert('ai win');
            location.reload();
        }
        var alldead2 = 0;
        for (var i = 0; i < this.AiShips.length; i++) {
            if (this.AiShips[i].state == ship_1.ShipState.Dead) {
                alldead2++;
            }
        }
        if (alldead2 >= this.humanShips.length) {
            alert('human win');
            location.reload();
        }
    };
    __decorate([
        decorators_1.log()
    ], Game.prototype, "drawGameInterface", null);
    return Game;
}());
exports.Game = Game;
var GameState;
(function (GameState) {
    GameState[GameState["GameInit"] = -1] = "GameInit";
    GameState[GameState["AiInit"] = 0] = "AiInit";
    GameState[GameState["HumanInit"] = 1] = "HumanInit";
    GameState[GameState["HumanRound"] = 3] = "HumanRound";
    GameState[GameState["AiRound"] = 4] = "AiRound";
    GameState[GameState["Ended"] = 5] = "Ended";
})(GameState = exports.GameState || (exports.GameState = {}));
