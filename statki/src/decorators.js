"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function log() {
    return function (target, name, descriptor) {
        var originalMethod = descriptor.value;
        descriptor.value = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var result = originalMethod.apply(this, args);
            var state = target.getstate.apply(this);
            console.log(target);
            console.log(name);
            console.log(descriptor);
            if (state == 4) {
                alert("Ruch: AI.");
            }
            else {
                alert("Ruch: GRACZA.");
            }
            return result;
        };
    };
}
exports.log = log;
