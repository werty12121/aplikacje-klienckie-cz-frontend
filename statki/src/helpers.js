"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Support = /** @class */ (function () {
    function Support() {
    }
    Support.randomFromIntRange = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    return Support;
}());
exports.Support = Support;
var Position2D = /** @class */ (function () {
    function Position2D(x, y) {
        this.x = x;
        this.y = y;
    }
    return Position2D;
}());
exports.Position2D = Position2D;
var BoardState;
(function (BoardState) {
    BoardState[BoardState["NORMAL"] = 0] = "NORMAL";
    BoardState[BoardState["HIT"] = 1] = "HIT";
})(BoardState = exports.BoardState || (exports.BoardState = {}));
