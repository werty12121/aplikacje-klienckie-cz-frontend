"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var helpers_1 = require("./helpers");
var helpers_2 = require("./helpers");
var ship_1 = require("./ship");
var Board = /** @class */ (function () {
    function Board(size) {
        this.size = size;
        this.board = [];
        for (var i = 0; i < this.size.y; i++) {
            this.board[i] = Array();
            for (var j = 0; j < this.size.x; j++) {
                this.board[i][j] = 0;
            }
        }
    }
    Board.prototype.checkNeighbourhood = function (pos) {
        var x = pos.x;
        var y = pos.y;
        var a0 = (this.board[y][x] == 0);
        var a1 = (x + 1 >= this.board[y].length || this.board[y][x + 1] == 0);
        var a2 = (x - 1 < 0 || this.board[y][x - 1] == 0);
        var a3 = (y + 1 >= this.board.length || this.board[y + 1][x] == 0);
        var a4 = (y - 1 < 0 || this.board[y - 1][x] == 0);
        var a5 = (x + 1 >= this.board[y].length || y + 1 >= this.board.length || this.board[y + 1][x + 1] == 0);
        var a6 = (x - 1 < 0 || y + 1 >= this.board.length || this.board[y + 1][x - 1] == 0);
        var a7 = (x - 1 < 0 || y - 1 < 0 || this.board[y - 1][x - 1] == 0);
        var a8 = (x + 1 >= this.board[y].length || y - 1 < 0 || this.board[y - 1][x + 1] == 0);
        if (!(a0 && a1 && a2 && a3 && a4 && a5 && a6 && a7 && a8)) {
            return false;
        }
        else {
            return true;
        }
    };
    Board.prototype.fillWithRandomShips = function (ships) {
        var ships_array = [];
        for (var i = 0; i < ships.length; i += 0) {
            var x = helpers_2.Support.randomFromIntRange(0, this.size.x - ships[i]);
            var y = helpers_2.Support.randomFromIntRange(0, this.size.x - ships[i]);
            var pozOrPion = helpers_2.Support.randomFromIntRange(0, 1);
            console.log("pozorpion " + pozOrPion);
            var temporaryBool = true;
            for (var c = 0; c < ships[i]; c++) {
                if (temporaryBool) {
                    if (pozOrPion == 0) {
                        temporaryBool = this.checkNeighbourhood(new helpers_1.Position2D(x + c, y));
                    }
                    else {
                        temporaryBool = this.checkNeighbourhood(new helpers_1.Position2D(x, y + c));
                    }
                }
            }
            console.log("init :" + temporaryBool + " " + pozOrPion);
            if (temporaryBool == true) {
                console.log("true" + ships[i]);
                var tempShip = new ship_1.Ship(ships[i], new helpers_1.Position2D(x, y));
                ships_array[ships_array.length] = tempShip;
                for (var c_1 = 0; c_1 < ships[i]; c_1++) {
                    if (pozOrPion == 0) {
                        this.board[y][x + c_1] = { ship_id: tempShip._id, pos_from_begin: c_1 };
                    }
                    else {
                        this.board[y + c_1][x] = { ship_id: tempShip._id, pos_from_begin: c_1 };
                    }
                }
                i++;
            }
        }
        return ships_array;
    };
    return Board;
}());
exports.Board = Board;
