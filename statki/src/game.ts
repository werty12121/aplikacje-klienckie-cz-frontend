import {Ship,ShipState} from './ship'
import {Support,Position2D,BoardState} from './helpers'
import {log} from './decorators'
import {Board} from './board'

export class Game{
    public state:GameState
    private boardSize
    private board:Board
    private humanShips
    private AiShips
    private shipsToInit
    private humanBoard:Board
    public selected
    constructor(boardSize){
        this.shipsToInit=[4,3,3,2,2,2,1,1,1,1]
        this.boardSize=boardSize
        this.AiShips=[]
        this.humanShips=[]
        this.selected=null
        this.state=GameState.GameInit
        this.board=new Board(this.boardSize)
        this.humanBoard=new Board(this.boardSize)
        this.state=GameState.AiInit
        this.initAiShips()
    }
    getstate(){
        return this.state
    }
    letHumanBeginInitShips(){
        this.drawHumanInitInterface()
    }
    initAiShips(){
        this.AiShips=this.board.fillWithRandomShips(this.shipsToInit)
        this.letHumanBeginInitShips()
        this.state=GameState.HumanInit
    }
    
    private drawHumanInitInterface(){
        var that=this
        window.oncontextmenu = function ()
        {
            if(that.selected!=null){
                that.selected.mode=!that.selected.mode
                
            }
            var arr=document.getElementsByClassName('hover_red')
                for(var h=0;h<arr.length;h++){
                    arr[h].className=""
                }
            var arr=document.getElementsByClassName('hover_green')
            for(var h=0;h<arr.length;h++){
                  arr[h].className=""
            }
            return false;     // cancel default menu
        }
      
        console.log("drawHumanInitInterface")
        this.clearInterface()
        let mainDiv=document.getElementById("main")
        var shipTable=document.createElement("Table")
        for(var i=0;i<this.shipsToInit.length;i++){
            var tr=document.createElement("Tr");
            for(var t=0;t<this.shipsToInit[i];t++){
                var td=document.createElement("Td")
            tr.setAttribute('posY',i.toString())
            td.setAttribute('posY',i.toString())
            td.setAttribute('lengthOfShip',this.shipsToInit[i])
            td.innerHTML=this.shipsToInit[i]
            tr.appendChild(td)
            }
            
            tr.onclick=(e)=>{
                var tlength,tpos
                let target = e.target as HTMLElement
                console.log(target.tagName)
                if(target.tagName!="TR"){
                    tlength=target.getAttribute('lengthOfShip')
                    target=target.parentElement
                    
                }else{
                    tlength=target.children[0].getAttribute('lengthOfShip')
                    
                }
                tpos=target.getAttribute('posy')
                    console.log(tpos)
                
                if(target.className=="selected"){
                    
                    var arr=document.getElementsByClassName('selected')
                    for(var h=0;h<arr.length;h++){
                        arr[h].className=""
                    }
                    target.className=""
                    
                    this.selected=null
                }else{
                    
                    var arr=document.getElementsByClassName('selected')
                    for(var h=0;h<arr.length;h++){
                        arr[h].className=""
                    }
                    target.className="selected"
                    this.selected={l:tlength,mode:true,pos:tpos}
                    console.log(this.selected)
                }
                    
            }
            tr.onmouseleave=(e)=>{
                let target = e.target as HTMLElement
                if( target.className!="selected")
                target.className=""
            }
            tr.onmouseenter=(e)=>{
                let target = e.target as HTMLElement
                if( target.className!="selected")
                target.className="hover"
            }
            
            shipTable.appendChild(tr)
        }
        mainDiv.appendChild(shipTable)

        var table=document.createElement("Table")
        table.setAttribute("id","planszadowyboru")
        for(let i=0;i<this.boardSize.y;i++){
            var tr=document.createElement("Tr");
            for(let j=0;j<this.boardSize.x;j++){
                var td=document.createElement("Td")
                td.setAttribute('posX',j.toString())
                td.setAttribute('posY',i.toString())
                td.setAttribute('id',i.toString()+'_'+j.toString())
                td.innerHTML="0"

                td.onmouseenter=(e)=>{
                    var target=e.target as HTMLElement 
                    //console.log(this.selected)
                    if(this.selected!=null){
                        var px=parseInt(target.getAttribute('posx'))
                        var py=parseInt(target.getAttribute('posy'))
                        var temp=true
                        for(var i=0;i<parseInt(this.selected.l);i++){
                            if(this.selected.mode){
                                
                                if(temp){
                                    if(px+parseInt(this.selected.l)>this.humanBoard.board[0].length){
                                        px=this.humanBoard.board[0].length-parseInt(this.selected.l)
                                    }
                                    temp=this.humanBoard.checkNeighbourhood(new Position2D(px+i,py))
                                }     
                            }else{
                                if(temp){
                                    if(py+parseInt(this.selected.l)>this.humanBoard.board.length){
                                        py=this.humanBoard.board.length-parseInt(this.selected.l)
                                    }
                                    temp=this.humanBoard.checkNeighbourhood(new Position2D(px,py+i))
                                }    
                            }
                           
                        }
                        if(temp){
                            podk="hover_green"
                        }else{
                            podk="hover_red"
                        }

                        for(var i=0;i<this.selected.l;i++){
                            var px=parseInt(target.getAttribute('posx'))
                            var py=parseInt(target.getAttribute('posy'))
                           
                            var s
                            var podk
                            
                            if(this.selected.mode){
                                
                                
                                if(px+parseInt(this.selected.l)>this.humanBoard.board[0].length){
                                    px=this.humanBoard.board[0].length-parseInt(this.selected.l)
                                }
                                s=document.getElementById(py+"_"+((px)+i))
                                
                            }else{
                                if(py+parseInt(this.selected.l)>this.humanBoard.board.length){
                                    py=this.humanBoard.board.length-parseInt(this.selected.l)
                                }
                                s=document.getElementById(((py)+i)+"_"+(px))
                            }
                            
                            //console.log((py)+"_"+((px)+i))
                            
                            if(s.className!='ship')
                                s.className=podk
                        }
                       
                    }
                }
                td.onclick=(e)=>{
                    var sc=false
                    var target=e.target as HTMLElement 
                    if(this.selected!=null&&target.className!='ship'){
                        var gpx=parseInt(target.getAttribute('posx'))
                        var gpy=parseInt(target.getAttribute('posy'))
                        var sh=new Ship(this.selected.l,new Position2D(gpx,gpy))
                        
                        for(var i=0;i<this.selected.l;i++){
                            var px=parseInt(target.getAttribute('posx'))
                            var py=parseInt(target.getAttribute('posy'))
                            var s
                            if(this.selected.mode){
                                if(px+parseInt(this.selected.l)>this.humanBoard.board[0].length){
                                    px=this.humanBoard.board[0].length-parseInt(this.selected.l)
                                }
                                
                                s=document.getElementById((py)+"_"+((px)+i))
                            }else{
                                if(py+parseInt(this.selected.l)>this.humanBoard.board.length){
                                    py=this.humanBoard.board.length-parseInt(this.selected.l)
                                }
                                
                                s=document.getElementById(((py)+i)+"_"+(px))
                            }
                            console.log(sh._id)
                            console.log(this.humanBoard.board)
                            if(target.className!='hover_red'){
                                s.className="ship"
                                if(this.selected.mode){
                                    this.humanBoard.board[py][px+i]={ship_id:sh._id,pos_from_begin:i}
                                }else{
                                    this.humanBoard.board[py+i][px]={ship_id:sh._id,pos_from_begin:i}
                                }
                                sc=true
                            }
                                
                        }
                       if(sc){
                        this.humanShips.push(sh)
                        var a = document.getElementsByTagName('td')
                        console.log("length: "+a.length)
                        var f=true
                        for(var i=0;i<a.length;i++){
                            if(f)
                            if(a[i].getAttribute('posy')==this.selected.pos&&a[i].getAttribute('id')==null){
                                
                                a[i].parentElement.remove()
                                f=false
                                
                            }
                        }
                        this.selected=null
                       }
                        
                        if(this.humanShips.length==this.shipsToInit.length){
                            this.state=GameState.HumanRound
                            this.drawGameInterface()
                        }
                        
                    }
                }
                td.onmouseleave=(e)=>{
                    var target=e.target as HTMLElement 
                    if(this.selected!=null){
                        for(var i=0;i<this.selected.l;i++){
                            var px=parseInt(target.getAttribute('posx'))
                            var py=parseInt(target.getAttribute('posy'))
                            var s
                            if(this.selected.mode){
                                if(px+parseInt(this.selected.l)>this.humanBoard.board[0].length){
                                    px=this.humanBoard.board[0].length-parseInt(this.selected.l)
                                }
                                s=document.getElementById((py)+"_"+((px)+i))
                            }else{
                                if(py+parseInt(this.selected.l)>this.humanBoard.board.length){
                                    py=this.humanBoard.board.length-parseInt(this.selected.l)
                                }
                                s=document.getElementById(((py)+i)+"_"+(px))
                            }
                            console.log((py)+"_"+((px)+i))
                            if(s.className!='ship')
                                s.className=""
                        }
                    }
                }

                tr.appendChild(td)
            }
            table.appendChild(tr)
        }
        mainDiv.appendChild(table)
    }
    private clearInterface(){
        let mainDiv=document.getElementById("main")
        mainDiv.innerHTML=""
    }
    @log()
    private drawGameInterface(){
        this.checkwinner()
        let mainDiv=document.getElementById("main")
        this.clearInterface()
        window.oncontextmenu=()=>{
            return true
        } 
       

        //HUMAN BOARD
        var table=document.createElement("Table")
        table.setAttribute("id","humanBoard")
        for(var i =0;i<this.humanBoard.board.length;i++){ 
            var tr=document.createElement("Tr");
            for(var j =0;j<this.humanBoard.board[i].length;j++){
                var td=document.createElement("Td")
                td.setAttribute('posX',j.toString())
                td.setAttribute('posY',i.toString())
                td.setAttribute('id',i.toString()+'_'+j.toString())
                if(this.humanBoard.board[i][j]==BoardState.HIT){
                    td.className='hover_gray'
                    td.innerHTML="0"
                }else if(this.humanBoard.board[i][j]!=0){
                    var temp_ship =Game.getShipFromArrayById(this.humanShips,this.humanBoard.board[i][j].ship_id) as Ship
                    switch(temp_ship.ship[this.humanBoard.board[i][j].pos_from_begin].state){
                        case ShipState.Normal:{
                            td.className='hover_green'
                            break;
                        }
                        case ShipState.Hit:{
                            td.className='hover_orange'
                            break;
                        }
                        case ShipState.Dead:{
                            td.className='hover_red'
                            break;
                        }
                    }
                    td.innerHTML='0'//(temp_ship._id).toString()
                }else{
                    td.innerHTML="0"
                }
                tr.appendChild(td)
            }
            table.appendChild(tr)
        }
        mainDiv.appendChild(table)



        //AI BOARD
        var table=document.createElement("Table")
        table.setAttribute("id","aiBoard")
        for(var i =0;i<this.board.board.length;i++){ 
            var tr=document.createElement("Tr");
            for(var j =0;j<this.board.board[i].length;j++){
                var td=document.createElement("Td")
                td.setAttribute('posX',j.toString())
                td.setAttribute('posY',i.toString())
                td.setAttribute('id','ai_'+i.toString()+'_'+j.toString())
                //console.log(this.board[i][j])
                switch(this.board.board[i][j])
                {
                    case BoardState.HIT:{
                        td.className="hover_gray"
                        break;
                    }
                    case 0:{
                        
                        break;
                    }
                    default:{
                        var tempship=Game.getShipFromArrayById(this.AiShips,this.board.board[i][j].ship_id) as Ship
                        //console.log(tempship.ship[this.board[i][j].pos_from_begin])
                        switch(tempship.ship[this.board.board[i][j].pos_from_begin].state){
                            case ShipState.Normal:{
                                console.log("Normal")
                                break;
                            } 
                            case ShipState.Hit:{
                                console.log("hit")
                                td.className="hover_orange"
                                break;
                            } 
                            case ShipState.Dead:{
                                console.log("dead")
                                td.className="hover_red"
                                break;
                            } 
                        }
                        break;
                    }
                }
                if(this.state==GameState.HumanRound){
                    // td.onmouseenter=(e)=>{
                        
                    //     var target=e.target as HTMLElement 
                    //     console.log("ai"+target.id)
                    //     var arr=document.getElementsByClassName('hover_khaki')
                    //     for(var h=0;h<arr.length;h++){
                    //         arr[h].className=""
                    //         var temp_pos_x=arr[h].id.split('_')[2]
                    //         var temp_pos_y=arr[h].id.split('_')[1]
                            
                    //     }
                    //     target.className='hover_khaki'
                    // }
                    td.onclick=(e)=>{
                        var target=e.target as HTMLElement 
                        var temp_pos_x=target.id.split('_')[2]
                        var temp_pos_y=target.id.split('_')[1]
                        switch(this.board.board[temp_pos_y][temp_pos_x]){
                            case 0:{
                                this.board.board[temp_pos_y][temp_pos_x]=BoardState.HIT
                                this.state=GameState.AiRound
                                break;
                            }
                            case BoardState.HIT:{
                                return
                                break;
                            }
                            default:{
                                
                                var temp_ship_2=Game.getShipFromArrayById(this.AiShips,this.board.board[temp_pos_y][temp_pos_x].ship_id) as Ship

                                if(temp_ship_2.ship[this.board.board[temp_pos_y][temp_pos_x].pos_from_begin].state==ShipState.Normal){
                                    console.log(this.board.board[temp_pos_y][temp_pos_x].pos_from_begin)
                                    temp_ship_2.hit(this.board.board[temp_pos_y][temp_pos_x].pos_from_begin)
                                    console.log(temp_ship_2.ship)
                                    this.state=GameState.AiRound
                                }
                                break;
                            }
                        }
                        var that=this
                        setTimeout(()=>{
                            that.drawGameInterface()
                        },10)
                        
                    }
                }

                tr.appendChild(td)
            }
            table.appendChild(tr)
        }
        mainDiv.appendChild(table)

        var that=this
        setTimeout(()=>{
        if(that.state==GameState.AiRound){
            var shooted=false
            while(!shooted){
                var x=Support.randomFromIntRange(0,that.boardSize.x-1)
                var y=Support.randomFromIntRange(0,that.boardSize.y-1)
                switch(that.humanBoard.board[y][x]){
                    case 0:{
                        that.humanBoard.board[y][x]=BoardState.HIT
                        shooted=true;
                        break;
                    }
                    case BoardState.HIT:{
                        break;
                    }
                    default:{
                        var temp_ship =Game.getShipFromArrayById(that.humanShips,that.humanBoard.board[y][x].ship_id) as Ship
                        if(temp_ship.ship[that.humanBoard.board[y][x].pos_from_begin].state==ShipState.Normal){
                            temp_ship.hit(that.humanBoard.board[y][x].pos_from_begin)
                            shooted=true;
                        }
                        break;
                    }
                }
            }
            that.state=GameState.HumanRound
            that.drawGameInterface()
        }
    },1000)
    }
    static getShipFromArrayById(array,id){
        for(var i=0;i<array.length;i++){
            if((array[i] as Ship)._id==id){
                return array[i]
            }
        }
        return null

    }
    private checkwinner(){
        var alldead=0
        for(var i=0;i<this.humanShips.length;i++){
            if(this.humanShips[i].state==ShipState.Dead){
                alldead++
            }
        }
        if(alldead>=this.humanShips.length){
            alert('ai win')
            location.reload();
        }

        var alldead2=0
        for(var i=0;i<this.AiShips.length;i++){
            if(this.AiShips[i].state==ShipState.Dead){
                alldead2++
            }
        }
        if(alldead2>=this.humanShips.length){
            alert('human win')
            location.reload();
        }
    }
}


export enum GameState{
    GameInit=-1,AiInit=0,HumanInit=1,HumanRound=3,AiRound=4,Ended=5
}