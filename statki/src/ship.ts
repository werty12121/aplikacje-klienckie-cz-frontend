import {Position2D} from './helpers'


export class Ship{
    public _id=Math.random()
    private position
    private length:number
    public ship
    public state

    
    constructor(length,position:Position2D){
        this.init(length,position)
    }

    
    init(length,position:Position2D){
        this.length=length
        this.position=position
        this.ship=[]
        this.state=ShipState.Normal
        for(var i=0;i<this.length;i++){
            this.ship[i]={state:ShipState.Normal}
        }
    }
    
    hit(pos:number){
        // let tempx=this.position.x-pos.x
        // let tempy=this.position.y-pos.y
        // let sPos:number
        // if(tempx>0){
        //     sPos=tempx
        // }else{
        //     sPos=tempy
        // }
        console.log(this.ship[pos])
        this.ship[pos].state=ShipState.Hit
        this.state=ShipState.Hit
        this.checkIfAlreadyKill()
    }
    checkIfAlreadyKill(){
        let temp=0
        for(var i=0;i<this.length;i++){
            if(this.ship[i].state==ShipState.Hit){
                temp++
            }
        }
        if(temp>=this.length){
            this.kill()
        }
    }

   
    kill(){
        this.state=ShipState.Dead
        for(var i=0;i<this.length;i++){
            this.ship[i].state=ShipState.Dead
        }
    }
}
export enum ShipState{
    Normal=0,Hit=1,Dead=2
}