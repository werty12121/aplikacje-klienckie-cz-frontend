export class Support{
    static randomFromIntRange(min,max){
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}
export class Position2D{
    public x:number;
    public y:number;
    constructor(x,y){
        this.x=x
        this.y=y
    }
    
}
export enum BoardState{
    NORMAL=0,HIT=1
}