"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Ship = /** @class */ (function () {
    function Ship(length, position) {
        this._id = Math.random();
        this.init(length, position);
    }
    Ship.prototype.init = function (length, position) {
        this.length = length;
        this.position = position;
        this.ship = [];
        this.state = ShipState.Normal;
        for (var i = 0; i < this.length; i++) {
            this.ship[i] = { state: ShipState.Normal };
        }
    };
    Ship.prototype.hit = function (pos) {
        // let tempx=this.position.x-pos.x
        // let tempy=this.position.y-pos.y
        // let sPos:number
        // if(tempx>0){
        //     sPos=tempx
        // }else{
        //     sPos=tempy
        // }
        console.log(this.ship[pos]);
        this.ship[pos].state = ShipState.Hit;
        this.state = ShipState.Hit;
        this.checkIfAlreadyKill();
    };
    Ship.prototype.checkIfAlreadyKill = function () {
        var temp = 0;
        for (var i = 0; i < this.length; i++) {
            if (this.ship[i].state == ShipState.Hit) {
                temp++;
            }
        }
        if (temp >= this.length) {
            this.kill();
        }
    };
    Ship.prototype.kill = function () {
        this.state = ShipState.Dead;
        for (var i = 0; i < this.length; i++) {
            this.ship[i].state = ShipState.Dead;
        }
    };
    return Ship;
}());
exports.Ship = Ship;
var ShipState;
(function (ShipState) {
    ShipState[ShipState["Normal"] = 0] = "Normal";
    ShipState[ShipState["Hit"] = 1] = "Hit";
    ShipState[ShipState["Dead"] = 2] = "Dead";
})(ShipState = exports.ShipState || (exports.ShipState = {}));
