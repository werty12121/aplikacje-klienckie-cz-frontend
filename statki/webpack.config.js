const path = require('path');
			module.exports = {
				entry: {
				  main: './src/main.js',
                  game: './src/game.js',
                  ship: './src/ship.js',
									helpers: './src/helpers.js',
									decorators: './src/decorators.js',
									board:'./src/board.js',
				},
				output: {
				  path: path.resolve(__dirname, './dist'),
				  filename: '[name].bundle.js'
                },
                watch:true
			  };