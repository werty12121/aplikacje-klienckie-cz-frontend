/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/ship.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/ship.js":
/*!*********************!*\
  !*** ./src/ship.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\r\nObject.defineProperty(exports, \"__esModule\", { value: true });\r\nvar Ship = /** @class */ (function () {\r\n    function Ship(length, position) {\r\n        this._id = Math.random();\r\n        this.init(length, position);\r\n    }\r\n    Ship.prototype.init = function (length, position) {\r\n        this.length = length;\r\n        this.position = position;\r\n        this.ship = [];\r\n        this.state = ShipState.Normal;\r\n        for (var i = 0; i < this.length; i++) {\r\n            this.ship[i] = { state: ShipState.Normal };\r\n        }\r\n    };\r\n    Ship.prototype.hit = function (pos) {\r\n        // let tempx=this.position.x-pos.x\r\n        // let tempy=this.position.y-pos.y\r\n        // let sPos:number\r\n        // if(tempx>0){\r\n        //     sPos=tempx\r\n        // }else{\r\n        //     sPos=tempy\r\n        // }\r\n        console.log(this.ship[pos]);\r\n        this.ship[pos].state = ShipState.Hit;\r\n        this.state = ShipState.Hit;\r\n        this.checkIfAlreadyKill();\r\n    };\r\n    Ship.prototype.checkIfAlreadyKill = function () {\r\n        var temp = 0;\r\n        for (var i = 0; i < this.length; i++) {\r\n            if (this.ship[i].state == ShipState.Hit) {\r\n                temp++;\r\n            }\r\n        }\r\n        if (temp >= this.length) {\r\n            this.kill();\r\n        }\r\n    };\r\n    Ship.prototype.kill = function () {\r\n        this.state = ShipState.Dead;\r\n        for (var i = 0; i < this.length; i++) {\r\n            this.ship[i].state = ShipState.Dead;\r\n        }\r\n    };\r\n    return Ship;\r\n}());\r\nexports.Ship = Ship;\r\nvar ShipState;\r\n(function (ShipState) {\r\n    ShipState[ShipState[\"Normal\"] = 0] = \"Normal\";\r\n    ShipState[ShipState[\"Hit\"] = 1] = \"Hit\";\r\n    ShipState[ShipState[\"Dead\"] = 2] = \"Dead\";\r\n})(ShipState = exports.ShipState || (exports.ShipState = {}));\r\n\n\n//# sourceURL=webpack:///./src/ship.js?");

/***/ })

/******/ });