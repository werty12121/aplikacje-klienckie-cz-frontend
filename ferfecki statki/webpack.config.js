const path = require('path');
			module.exports = {
				entry: {
				  glowny: './src/glowny.js',
          statek: './src/statek.js',
					plansza:'./src/plansza.js',
				},
				output: {
				  path: path.resolve(__dirname, './dist'),
				  filename: '[name].bundle.js'
                },
                watch:true
			  };