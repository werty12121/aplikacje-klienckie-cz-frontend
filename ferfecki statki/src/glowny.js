"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var plansza_1 = require("./plansza");
var statek_1 = require("./statek");
var Gra = /** @class */ (function () {
    function Gra(x, y) {
        this.x = x;
        this.y = y;
        this.ruch = "gracza";
        this.planszaGracza = new plansza_1.Plansza(this.x, this.y);
        this.planszaKomputera = new plansza_1.Plansza(this.x, this.y);
        this.planszaKomputera.rozstawStatkiKomputera();
        this.zaznaczony = null;
        this.rozstawienieStatkówGracza();
    }
    Gra.prototype.rozpocznijGre = function () {
        var wszystko = document.getElementById('wszystko');
        wszystko.innerHTML = '';
        var planszaGraczaNaEkranie = document.createElement('div');
        planszaGraczaNaEkranie.setAttribute('id', 'planszaGraczaNaEkranie');
        var planszaKomputeraNaEkranie = document.createElement('div');
        planszaKomputeraNaEkranie.setAttribute('id', 'planszaKomputeraNaEkranie');
        wszystko.append(planszaGraczaNaEkranie);
        wszystko.append(planszaKomputeraNaEkranie);
        this.rysujInterfaceGry();
    };
    Gra.prototype.czyscInetrfacy = function () {
        var planszaGraczaNaEkranie = document.getElementById('planszaGraczaNaEkranie');
        planszaGraczaNaEkranie.innerHTML = '';
        var planszaKomputeraNaEkranie = document.getElementById('planszaKomputeraNaEkranie');
        planszaKomputeraNaEkranie.innerHTML = '';
    };
    Gra.prototype.czyktoswygral = function () {
        var liczikzywychGracz = 0;
        for (var i = 0; i < this.planszaGracza.plansza.length; i++) {
            for (var j = 0; j < this.planszaGracza.plansza[i].length; j++) {
                switch (this.planszaGracza.plansza[i][j]) {
                    case 0:
                        break;
                    case "trafiony":
                        break;
                    default: {
                        var temp_ship = this.planszaGracza.plansza[i][j].statek;
                        if (temp_ship.statek[this.planszaGracza.plansza[i][j].ktorepole] == "Ok")
                            liczikzywychGracz++;
                        break;
                    }
                }
            }
            if (this.planszaGracza.plansza.length <= i + 1) {
                if (liczikzywychGracz == 0) {
                    alert("Komputer wygrał . Cienias z ciebie!");
                    location.reload();
                }
            }
        }
        var liczikzywychKomputer = 0;
        for (var i = 0; i < this.planszaKomputera.plansza.length; i++) {
            for (var j = 0; j < this.planszaKomputera.plansza[i].length; j++) {
                switch (this.planszaKomputera.plansza[i][j]) {
                    case 0:
                        break;
                    case "trafiony":
                        break;
                    default: {
                        var temp_ship = this.planszaKomputera.plansza[i][j].statek;
                        console.log(temp_ship.statek[this.planszaKomputera.plansza[i][j].ktorepole]);
                        if (temp_ship.statek[this.planszaKomputera.plansza[i][j].ktorepole] == "Ok")
                            liczikzywychKomputer++;
                        break;
                    }
                }
            }
            if (this.planszaKomputera.plansza.length <= i + 1) {
                if (liczikzywychKomputer == 0) {
                    alert("Gracz wygrał .");
                    location.reload();
                }
            }
        }
    };
    Gra.prototype.rysujInterfaceGry = function () {
        var _this = this;
        alert("ruch " + this.ruch);
        var mainDiv = document.getElementById("planszaGraczaNaEkranie");
        var mainDiv2 = document.getElementById("planszaKomputeraNaEkranie");
        this.czyscInetrfacy();
        window.oncontextmenu = function () {
            return true;
        };
        //HUMAN BOARD
        var table = document.createElement("Table");
        table.setAttribute("id", "humanBoard");
        for (var i = 0; i < this.planszaGracza.plansza.length; i++) {
            var tr = document.createElement("Tr");
            for (var j = 0; j < this.planszaGracza.plansza[i].length; j++) {
                var td = document.createElement("Td");
                td.setAttribute('pozycjaX', j.toString());
                td.setAttribute('pozycjaY', i.toString());
                td.setAttribute('id', i.toString() + '_' + j.toString());
                if (this.planszaGracza.plansza[i][j] == "trafiony") {
                    td.className = 'trafiony';
                    td.innerHTML = "0";
                }
                else if (this.planszaGracza.plansza[i][j] != 0) {
                    var temp_ship = this.planszaGracza.plansza[i][j].statek;
                    switch (temp_ship.statek[this.planszaGracza.plansza[i][j].ktorepole]) {
                        case "Ok": {
                            td.className = 'nieTrafionyStatek';
                            break;
                        }
                        case "NieOk": {
                            td.className = 'trafionyStatek';
                            break;
                        }
                    }
                    td.innerHTML = '0'; //(temp_ship._id).toString()
                }
                else {
                    td.innerHTML = "0";
                }
                tr.appendChild(td);
            }
            table.appendChild(tr);
        }
        mainDiv.appendChild(table);
        //AI BOARD
        var table = document.createElement("Table");
        table.setAttribute("id", "aiBoard");
        for (var i = 0; i < this.planszaKomputera.plansza.length; i++) {
            var tr = document.createElement("Tr");
            for (var j = 0; j < this.planszaKomputera.plansza[i].length; j++) {
                var td = document.createElement("Td");
                td.setAttribute('pozycjaX', j.toString());
                td.setAttribute('pozycjaY', i.toString());
                td.setAttribute('id', 'komputer_' + i.toString() + '_' + j.toString());
                //console.log(this.board[i][j])
                switch (this.planszaKomputera.plansza[i][j]) {
                    case "trafiony": {
                        td.className = "trafiony";
                        break;
                    }
                    case 0: {
                        break;
                    }
                    default: {
                        var tempship = this.planszaKomputera.plansza[i][j].statek;
                        switch (tempship.statek[this.planszaKomputera.plansza[i][j].ktorepole]) {
                            case "Ok": {
                                break;
                            }
                            case "NieOk": {
                                td.className = "trafionyStatek";
                                break;
                            }
                        }
                        break;
                    }
                }
                if (this.ruch == 'gracza') {
                    td.onclick = function (e) {
                        var target = e.target;
                        var temp_pos_x = target.id.split('_')[2];
                        var temp_pos_y = target.id.split('_')[1];
                        switch (_this.planszaKomputera.plansza[temp_pos_y][temp_pos_x]) {
                            case 0: {
                                _this.planszaKomputera.plansza[temp_pos_y][temp_pos_x] = "trafiony";
                                _this.ruch = "komputera";
                                break;
                            }
                            case "trafiony": {
                                return;
                                break;
                            }
                            default: {
                                var temp_ship_2 = _this.planszaKomputera.plansza[temp_pos_y][temp_pos_x].statek;
                                console.log(temp_ship_2.statek[_this.planszaKomputera.plansza[temp_pos_y][temp_pos_x].ktorepole]);
                                if (temp_ship_2.statek[_this.planszaKomputera.plansza[temp_pos_y][temp_pos_x].ktorepole] == "Ok") {
                                    temp_ship_2.trafienie(_this.planszaKomputera.plansza[temp_pos_y][temp_pos_x].ktorepole);
                                }
                                _this.ruch = "komputera";
                                break;
                            }
                        }
                        var that = _this;
                        setTimeout(function () {
                            that.rysujInterfaceGry();
                        }, 10);
                    };
                }
                tr.appendChild(td);
            }
            table.appendChild(tr);
        }
        mainDiv2.appendChild(table);
        var that = this;
        if (that.ruch == 'komputera') {
            setTimeout(function () {
                var shooted = true;
                while (shooted) {
                    var x = _this.randomnumber(0, that.x - 1);
                    var y = _this.randomnumber(0, that.y - 1);
                    switch (that.planszaGracza.plansza[y][x]) {
                        case 0: {
                            that.planszaGracza.plansza[y][x] = "trafiony";
                            shooted = false;
                            break;
                        }
                        case "trafiony": {
                            break;
                        }
                        default: {
                            var temp_ship = that.planszaGracza.plansza[y][x].statek;
                            if (temp_ship.statek[that.planszaGracza.plansza[y][x].ktorepole] == 'Ok') {
                                temp_ship.trafienie(that.planszaGracza.plansza[y][x].ktorepole);
                                shooted = false;
                            }
                            break;
                        }
                    }
                }
                that.ruch = 'gracza';
                that.rysujInterfaceGry();
            }, 1000);
        }
        this.czyktoswygral();
    };
    Gra.prototype.rozstawienieStatkówGracza = function () {
        var _this = this;
        var statki = [4, 3, 3, 2, 2, 2, 1, 1, 1, 1];
        var that = this;
        window.oncontextmenu = function () {
            if (that.zaznaczony != null) {
                if (that.zaznaczony.mode == 'poz')
                    that.zaznaczony.mode = 'pion';
                else
                    that.zaznaczony.mode = 'poz';
            }
            var arr = document.getElementsByClassName('najechany_zle');
            for (var h = 0; h < arr.length; h++) {
                arr[h].className = "";
            }
            var arr = document.getElementsByClassName('najechany_ok');
            for (var h = 0; h < arr.length; h++) {
                arr[h].className = "";
            }
            return false;
        };
        var mainDiv = document.getElementById("statki");
        var mainDiv2 = document.getElementById("plansze");
        var shipTable = document.createElement("Table");
        this.zaznaczony = { l: 4, mode: 'poz', pos: 0 };
        for (var i = 0; i < statki.length; i++) {
            var tr = document.createElement("Tr");
            tr.setAttribute('pozycjaY', i.toString());
            for (var t = 0; t < statki[i]; t++) {
                var td = document.createElement("Td");
                td.setAttribute('pozycjaY', i.toString());
                td.setAttribute('dlugoststatku', statki[i].toString());
                td.innerHTML = statki[i].toString();
                tr.appendChild(td);
            }
            if (i == 0) {
                tr.className = "zaznaczony";
            }
            tr.onclick = function (e) {
                var dlugoscstatku, tpos;
                var target = e.target;
                if (target.tagName != "TR") {
                    dlugoscstatku = target.getAttribute('dlugoststatku');
                    target = target.parentElement;
                }
                else {
                    dlugoscstatku = target.children[0].getAttribute('dlugoststatku');
                }
                tpos = target.getAttribute('pozycjay');
                if (target.className == "zaznaczony") {
                    var arr = document.getElementsByClassName('zaznaczony');
                    for (var h = 0; h < arr.length; h++) {
                        arr[h].className = "";
                    }
                    target.className = "";
                    _this.zaznaczony = null;
                }
                else {
                    var arr = document.getElementsByClassName('zaznaczony');
                    for (var h = 0; h < arr.length; h++) {
                        arr[h].className = "";
                    }
                    target.className = "zaznaczony";
                    _this.zaznaczony = { l: dlugoscstatku, mode: 'poz', pos: tpos };
                }
            };
            tr.onmouseleave = function (e) {
                var target = e.target;
                if (target.className != "zaznaczony")
                    target.className = "";
            };
            tr.onmouseenter = function (e) {
                var target = e.target;
                if (target.className != "zaznaczony")
                    target.className = "najechany_ok";
            };
            shipTable.appendChild(tr);
        }
        mainDiv.appendChild(shipTable);
        var table = document.createElement("Table");
        table.setAttribute("id", "planszadowyboru");
        for (var i_1 = 0; i_1 < this.y; i_1++) {
            var tr = document.createElement("Tr");
            for (var j = 0; j < this.x; j++) {
                var td = document.createElement("Td");
                td.setAttribute('pozycjaX', j.toString());
                td.setAttribute('pozycjaY', i_1.toString());
                td.setAttribute('id', i_1.toString() + '_' + j.toString());
                td.innerHTML = "0";
                td.onmouseenter = function (e) {
                    var target = e.target;
                    if (_this.zaznaczony != null) {
                        var px = parseInt(target.getAttribute('pozycjaX'));
                        var py = parseInt(target.getAttribute('pozycjay'));
                        var temp = true;
                        for (var i = 0; i < parseInt(_this.zaznaczony.l); i++) {
                            if (_this.zaznaczony.mode == 'poz') {
                                if (temp) {
                                    if (px + parseInt(_this.zaznaczony.l) > _this.planszaGracza.plansza[0].length) {
                                        px = _this.planszaGracza.plansza[0].length - parseInt(_this.zaznaczony.l);
                                    }
                                    temp = _this.planszaGracza.sprawzWszystkie(px + i, py);
                                }
                            }
                            else {
                                if (temp) {
                                    if (py + parseInt(_this.zaznaczony.l) > _this.planszaGracza.plansza.length) {
                                        py = _this.planszaGracza.plansza.length - parseInt(_this.zaznaczony.l);
                                    }
                                    temp = _this.planszaGracza.sprawzWszystkie(px, py + i);
                                }
                            }
                        }
                        if (temp) {
                            podk = "najechany_ok";
                        }
                        else {
                            podk = "najechany_zle";
                        }
                        for (var i = 0; i < _this.zaznaczony.l; i++) {
                            var px = parseInt(target.getAttribute('pozycjaX'));
                            var py = parseInt(target.getAttribute('pozycjay'));
                            var s;
                            var podk;
                            if (_this.zaznaczony.mode == 'poz') {
                                if (px + parseInt(_this.zaznaczony.l) > _this.planszaGracza.plansza[0].length) {
                                    px = _this.planszaGracza.plansza[0].length - parseInt(_this.zaznaczony.l);
                                }
                                s = document.getElementById(py + "_" + ((px) + i));
                            }
                            else {
                                if (py + parseInt(_this.zaznaczony.l) > _this.planszaGracza.plansza.length) {
                                    py = _this.planszaGracza.plansza.length - parseInt(_this.zaznaczony.l);
                                }
                                s = document.getElementById(((py) + i) + "_" + (px));
                            }
                            if (s.className != 'statek')
                                s.className = podk;
                        }
                    }
                };
                td.onclick = function (e) {
                    var sc = false;
                    var target = e.target;
                    if (_this.zaznaczony != null && target.className != 'statek') {
                        var gpx = parseInt(target.getAttribute('pozycjX'));
                        var gpy = parseInt(target.getAttribute('pozycjay'));
                        var sh = new statek_1.Statek(gpx, gpy, _this.zaznaczony.l);
                        for (var i = 0; i < _this.zaznaczony.l; i++) {
                            var px = parseInt(target.getAttribute('pozycjaX'));
                            var py = parseInt(target.getAttribute('pozycjay'));
                            var s;
                            if (_this.zaznaczony.mode == 'poz') {
                                if (px + parseInt(_this.zaznaczony.l) > _this.planszaGracza.plansza[0].length) {
                                    px = _this.planszaGracza.plansza[0].length - parseInt(_this.zaznaczony.l);
                                }
                                s = document.getElementById((py) + "_" + ((px) + i));
                            }
                            else {
                                if (py + parseInt(_this.zaznaczony.l) > _this.planszaGracza.plansza.length) {
                                    py = _this.planszaGracza.plansza.length - parseInt(_this.zaznaczony.l);
                                }
                                s = document.getElementById(((py) + i) + "_" + (px));
                            }
                            if (target.className != 'najechany_zle') {
                                s.className = "statek";
                                if (_this.zaznaczony.mode == 'poz') {
                                    _this.planszaGracza.plansza[py][px + i] = { statek: sh, ktorepole: i };
                                }
                                else {
                                    _this.planszaGracza.plansza[py + i][px] = { statek: sh, ktorepole: i };
                                }
                                sc = true;
                            }
                        }
                        if (sc) {
                            var a = document.getElementsByTagName('td');
                            var f = true;
                            for (var i = 0; i < a.length; i++) {
                                if (f)
                                    if (a[i].getAttribute('pozycjay') == _this.zaznaczony.pos && a[i].getAttribute('id') == null) {
                                        a[i].parentElement.remove();
                                        f = false;
                                    }
                            }
                            _this.zaznaczony = null;
                        }
                        var d = mainDiv.getElementsByTagName('tr');
                        if (d.length == 0) {
                            _this.ruch = "gracza";
                            _this.rozpocznijGre();
                        }
                    }
                };
                td.onmouseleave = function (e) {
                    var target = e.target;
                    if (_this.zaznaczony != null) {
                        for (var i = 0; i < _this.zaznaczony.l; i++) {
                            var px = parseInt(target.getAttribute('pozycjaX'));
                            var py = parseInt(target.getAttribute('pozycjay'));
                            var s;
                            if (_this.zaznaczony.mode == 'poz') {
                                if (px + parseInt(_this.zaznaczony.l) > _this.planszaGracza.plansza[0].length) {
                                    px = _this.planszaGracza.plansza[0].length - parseInt(_this.zaznaczony.l);
                                }
                                s = document.getElementById((py) + "_" + ((px) + i));
                            }
                            else {
                                if (py + parseInt(_this.zaznaczony.l) > _this.planszaGracza.plansza.length) {
                                    py = _this.planszaGracza.plansza.length - parseInt(_this.zaznaczony.l);
                                }
                                s = document.getElementById(((py) + i) + "_" + (px));
                            }
                            if (s.className != 'statek')
                                s.className = "";
                        }
                    }
                };
                tr.appendChild(td);
            }
            table.appendChild(tr);
        }
        mainDiv2.appendChild(table);
    };
    Gra.prototype.randomnumber = function (a, b) {
        return Math.floor(Math.random() * (b - a + 1)) + a;
    };
    return Gra;
}());
document.addEventListener("DOMContentLoaded", function () {
    new Gra(10, 10);
});
