export class Statek{
    public x
    public y
    public dlugosc
    public statek
    constructor(x,y,dlugosc){
        this.x=x
        this.y=y
        this.dlugosc=dlugosc
        this.statek=[]
        for(var i=0;i<dlugosc;i++){
            this.statek[i]="Ok"
        }
    }
    czyOK(){
        var s=0
        for(var i=0;i<this.dlugosc;i++){
            if(this.statek[i]=="Ok"){
                s++
            }
        }
        return s/this.dlugosc
    }
    @trafiono()
    trafienie(gdzie){
        this.statek[gdzie]="NieOk"
    }
}
function trafiono(){
    return (target: any, name: string, descriptor: any)=>{
        var originalMethod = descriptor.value;
        descriptor.value = function (...args: any[]) {
            var result = originalMethod.apply(this, args);
            alert("trafiono STATEK")
           
            return result;
        }
    }
}