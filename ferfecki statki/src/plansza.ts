import {Statek} from './statek'


export class Plansza{
    private x:number;
    private y:number;
    public plansza;
    constructor(x:number,y:number){
        this.x=x;
        this.y=y;
        this.zerujplansze()
    }
    private zerujplansze():void{
        this.plansza=[];
        for(let i=0;i<this.y;i++){
            this.plansza[i]=[];
            for(let j=0; j<this.x; j++){
                this.plansza[i][j]=0
            }
        }
    }
    sprawzWszystkie(x:number,y:number):boolean{
        try{
            return this.sprawdzSrodek(x,y)&&this.sprawdzGora(x,y)&&this.sprawdzGoraLewa(x,y)&&this.sprawdzGoraPrawa(x,y)&&this.sprawdzDol(x,y)&&this.sprawdzDolLewa(x,y)&&this.sprawdzDolPrawa(x,y)&&this.sprawdzLewa(x,y)&&this.sprawdzPrawa(x,y)
        }catch(e){
            return true
        }
    }
    sprawdzGora(x:number,y:number):boolean{
        return y+1>=this.plansza.length||this.plansza[y+1][x]==0
    }
    sprawdzGoraLewa(x:number,y:number):boolean{
        return x-1<0||y+1>=this.plansza.length||this.plansza[y+1][x-1]==0
    }
    sprawdzGoraPrawa(x:number,y:number):boolean{
        return x+1>=this.plansza[y].length||y+1>=this.plansza.length||this.plansza[y+1][x+1]==0
    }
    sprawdzDol(x:number,y:number):boolean{
        return y-1<0||this.plansza[y-1][x]==0
    }
    sprawdzDolLewa(x:number,y:number):boolean{
        return x-1<0||y-1<0||this.plansza[y-1][x-1]==0
    }
    sprawdzDolPrawa(x:number,y:number):boolean{
        return x+1>=this.plansza[y].length||y-1<0||this.plansza[y-1][x+1]==0
    }
    sprawdzLewa(x:number,y:number):boolean{
        return x-1<0||this.plansza[y][x-1]==0
    }
    sprawdzPrawa(x:number,y:number):boolean{
        return x+1>=this.plansza[y].length||this.plansza[y][x+1]==0
    }
    sprawdzSrodek(x:number,y:number):boolean{
        return this.plansza[y][x]==0
    }
    rozstawStatkiKomputera(){
        var statki=[4,3,3,2,2,2,1,1,1,1];
        for(var i=0;i<statki.length;i+=0){
            var rozmieszczenie=this.randomnumber(0,1);
            var x=this.randomnumber(0,this.x-statki[i]);
            var y=this.randomnumber(0,this.x-statki[i]);
                var czymoznawstawic=true;
                for(var j=0;j<statki[i];j++){
                    if(czymoznawstawic){
                        if(rozmieszczenie==1){
                            czymoznawstawic=this.sprawzWszystkie(x+j,y)
                        }else{
                            czymoznawstawic=this.sprawzWszystkie(x,y+j)
                        }
                    }
                }
                if(czymoznawstawic==true){
                    var statek=new Statek(x,y,statki[i]);
                    for(let j=0;j<statki[i];j++){
                        if(rozmieszczenie==1){
                            this.plansza[y][x+j]={statek:statek,ktorepole:j}
                        }else{
                            this.plansza[y+j][x]={statek:statek,ktorepole:j}
                        }
                    }
                    i+=1
                }
        }
    }
    randomnumber(a,b){
        return Math.floor(Math.random() * (b - a + 1)) + a;
    }
}