"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var statek_1 = require("./statek");
var Plansza = /** @class */ (function () {
    function Plansza(x, y) {
        this.x = x;
        this.y = y;
        this.zerujplansze();
    }
    Plansza.prototype.zerujplansze = function () {
        this.plansza = [];
        for (var i = 0; i < this.y; i++) {
            this.plansza[i] = [];
            for (var j = 0; j < this.x; j++) {
                this.plansza[i][j] = 0;
            }
        }
    };
    Plansza.prototype.sprawzWszystkie = function (x, y) {
        try {
            return this.sprawdzSrodek(x, y) && this.sprawdzGora(x, y) && this.sprawdzGoraLewa(x, y) && this.sprawdzGoraPrawa(x, y) && this.sprawdzDol(x, y) && this.sprawdzDolLewa(x, y) && this.sprawdzDolPrawa(x, y) && this.sprawdzLewa(x, y) && this.sprawdzPrawa(x, y);
        }
        catch (e) {
            return true;
        }
    };
    Plansza.prototype.sprawdzGora = function (x, y) {
        return y + 1 >= this.plansza.length || this.plansza[y + 1][x] == 0;
    };
    Plansza.prototype.sprawdzGoraLewa = function (x, y) {
        return x - 1 < 0 || y + 1 >= this.plansza.length || this.plansza[y + 1][x - 1] == 0;
    };
    Plansza.prototype.sprawdzGoraPrawa = function (x, y) {
        return x + 1 >= this.plansza[y].length || y + 1 >= this.plansza.length || this.plansza[y + 1][x + 1] == 0;
    };
    Plansza.prototype.sprawdzDol = function (x, y) {
        return y - 1 < 0 || this.plansza[y - 1][x] == 0;
    };
    Plansza.prototype.sprawdzDolLewa = function (x, y) {
        return x - 1 < 0 || y - 1 < 0 || this.plansza[y - 1][x - 1] == 0;
    };
    Plansza.prototype.sprawdzDolPrawa = function (x, y) {
        return x + 1 >= this.plansza[y].length || y - 1 < 0 || this.plansza[y - 1][x + 1] == 0;
    };
    Plansza.prototype.sprawdzLewa = function (x, y) {
        return x - 1 < 0 || this.plansza[y][x - 1] == 0;
    };
    Plansza.prototype.sprawdzPrawa = function (x, y) {
        return x + 1 >= this.plansza[y].length || this.plansza[y][x + 1] == 0;
    };
    Plansza.prototype.sprawdzSrodek = function (x, y) {
        return this.plansza[y][x] == 0;
    };
    Plansza.prototype.rozstawStatkiKomputera = function () {
        var statki = [4, 3, 3, 2, 2, 2, 1, 1, 1, 1];
        for (var i = 0; i < statki.length; i += 0) {
            var rozmieszczenie = this.randomnumber(0, 1);
            var x = this.randomnumber(0, this.x - statki[i]);
            var y = this.randomnumber(0, this.x - statki[i]);
            var czymoznawstawic = true;
            for (var j = 0; j < statki[i]; j++) {
                if (czymoznawstawic) {
                    if (rozmieszczenie == 1) {
                        czymoznawstawic = this.sprawzWszystkie(x + j, y);
                    }
                    else {
                        czymoznawstawic = this.sprawzWszystkie(x, y + j);
                    }
                }
            }
            if (czymoznawstawic == true) {
                var statek = new statek_1.Statek(x, y, statki[i]);
                for (var j_1 = 0; j_1 < statki[i]; j_1++) {
                    if (rozmieszczenie == 1) {
                        this.plansza[y][x + j_1] = { statek: statek, ktorepole: j_1 };
                    }
                    else {
                        this.plansza[y + j_1][x] = { statek: statek, ktorepole: j_1 };
                    }
                }
                i += 1;
            }
        }
    };
    Plansza.prototype.randomnumber = function (a, b) {
        return Math.floor(Math.random() * (b - a + 1)) + a;
    };
    return Plansza;
}());
exports.Plansza = Plansza;
