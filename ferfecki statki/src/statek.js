"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Statek = /** @class */ (function () {
    function Statek(x, y, dlugosc) {
        this.x = x;
        this.y = y;
        this.dlugosc = dlugosc;
        this.statek = [];
        for (var i = 0; i < dlugosc; i++) {
            this.statek[i] = "Ok";
        }
    }
    Statek.prototype.czyOK = function () {
        var s = 0;
        for (var i = 0; i < this.dlugosc; i++) {
            if (this.statek[i] == "Ok") {
                s++;
            }
        }
        return s / this.dlugosc;
    };
    Statek.prototype.trafienie = function (gdzie) {
        this.statek[gdzie] = "NieOk";
    };
    __decorate([
        trafiono()
    ], Statek.prototype, "trafienie", null);
    return Statek;
}());
exports.Statek = Statek;
function trafiono() {
    return function (target, name, descriptor) {
        var originalMethod = descriptor.value;
        descriptor.value = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var result = originalMethod.apply(this, args);
            alert("trafiono STATEK");
            return result;
        };
    };
}
