/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/plansza.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/plansza.js":
/*!************************!*\
  !*** ./src/plansza.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\r\nObject.defineProperty(exports, \"__esModule\", { value: true });\r\nvar statek_1 = __webpack_require__(/*! ./statek */ \"./src/statek.js\");\r\nvar Plansza = /** @class */ (function () {\r\n    function Plansza(x, y) {\r\n        this.x = x;\r\n        this.y = y;\r\n        this.zerujplansze();\r\n    }\r\n    Plansza.prototype.zerujplansze = function () {\r\n        this.plansza = [];\r\n        for (var i = 0; i < this.y; i++) {\r\n            this.plansza[i] = [];\r\n            for (var j = 0; j < this.x; j++) {\r\n                this.plansza[i][j] = 0;\r\n            }\r\n        }\r\n    };\r\n    Plansza.prototype.sprawzWszystkie = function (x, y) {\r\n        try {\r\n            return this.sprawdzSrodek(x, y) && this.sprawdzGora(x, y) && this.sprawdzGoraLewa(x, y) && this.sprawdzGoraPrawa(x, y) && this.sprawdzDol(x, y) && this.sprawdzDolLewa(x, y) && this.sprawdzDolPrawa(x, y) && this.sprawdzLewa(x, y) && this.sprawdzPrawa(x, y);\r\n        }\r\n        catch (e) {\r\n            return true;\r\n        }\r\n    };\r\n    Plansza.prototype.sprawdzGora = function (x, y) {\r\n        return y + 1 >= this.plansza.length || this.plansza[y + 1][x] == 0;\r\n    };\r\n    Plansza.prototype.sprawdzGoraLewa = function (x, y) {\r\n        return x - 1 < 0 || y + 1 >= this.plansza.length || this.plansza[y + 1][x - 1] == 0;\r\n    };\r\n    Plansza.prototype.sprawdzGoraPrawa = function (x, y) {\r\n        return x + 1 >= this.plansza[y].length || y + 1 >= this.plansza.length || this.plansza[y + 1][x + 1] == 0;\r\n    };\r\n    Plansza.prototype.sprawdzDol = function (x, y) {\r\n        return y - 1 < 0 || this.plansza[y - 1][x] == 0;\r\n    };\r\n    Plansza.prototype.sprawdzDolLewa = function (x, y) {\r\n        return x - 1 < 0 || y - 1 < 0 || this.plansza[y - 1][x - 1] == 0;\r\n    };\r\n    Plansza.prototype.sprawdzDolPrawa = function (x, y) {\r\n        return x + 1 >= this.plansza[y].length || y - 1 < 0 || this.plansza[y - 1][x + 1] == 0;\r\n    };\r\n    Plansza.prototype.sprawdzLewa = function (x, y) {\r\n        return x - 1 < 0 || this.plansza[y][x - 1] == 0;\r\n    };\r\n    Plansza.prototype.sprawdzPrawa = function (x, y) {\r\n        return x + 1 >= this.plansza[y].length || this.plansza[y][x + 1] == 0;\r\n    };\r\n    Plansza.prototype.sprawdzSrodek = function (x, y) {\r\n        return this.plansza[y][x] == 0;\r\n    };\r\n    Plansza.prototype.rozstawStatkiKomputera = function () {\r\n        var statki = [4, 3, 3, 2, 2, 2, 1, 1, 1, 1];\r\n        for (var i = 0; i < statki.length; i += 0) {\r\n            var rozmieszczenie = this.randomnumber(0, 1);\r\n            var x = this.randomnumber(0, this.x - statki[i]);\r\n            var y = this.randomnumber(0, this.x - statki[i]);\r\n            var czymoznawstawic = true;\r\n            for (var j = 0; j < statki[i]; j++) {\r\n                if (czymoznawstawic) {\r\n                    if (rozmieszczenie == 1) {\r\n                        czymoznawstawic = this.sprawzWszystkie(x + j, y);\r\n                    }\r\n                    else {\r\n                        czymoznawstawic = this.sprawzWszystkie(x, y + j);\r\n                    }\r\n                }\r\n            }\r\n            if (czymoznawstawic == true) {\r\n                var statek = new statek_1.Statek(x, y, statki[i]);\r\n                for (var j_1 = 0; j_1 < statki[i]; j_1++) {\r\n                    if (rozmieszczenie == 1) {\r\n                        this.plansza[y][x + j_1] = { statek: statek, ktorepole: j_1 };\r\n                    }\r\n                    else {\r\n                        this.plansza[y + j_1][x] = { statek: statek, ktorepole: j_1 };\r\n                    }\r\n                }\r\n                i += 1;\r\n            }\r\n        }\r\n    };\r\n    Plansza.prototype.randomnumber = function (a, b) {\r\n        return Math.floor(Math.random() * (b - a + 1)) + a;\r\n    };\r\n    return Plansza;\r\n}());\r\nexports.Plansza = Plansza;\r\n\n\n//# sourceURL=webpack:///./src/plansza.js?");

/***/ }),

/***/ "./src/statek.js":
/*!***********************!*\
  !*** ./src/statek.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\r\nvar __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {\r\n    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;\r\n    if (typeof Reflect === \"object\" && typeof Reflect.decorate === \"function\") r = Reflect.decorate(decorators, target, key, desc);\r\n    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;\r\n    return c > 3 && r && Object.defineProperty(target, key, r), r;\r\n};\r\nObject.defineProperty(exports, \"__esModule\", { value: true });\r\nvar Statek = /** @class */ (function () {\r\n    function Statek(x, y, dlugosc) {\r\n        this.x = x;\r\n        this.y = y;\r\n        this.dlugosc = dlugosc;\r\n        this.statek = [];\r\n        for (var i = 0; i < dlugosc; i++) {\r\n            this.statek[i] = \"Ok\";\r\n        }\r\n    }\r\n    Statek.prototype.czyOK = function () {\r\n        var s = 0;\r\n        for (var i = 0; i < this.dlugosc; i++) {\r\n            if (this.statek[i] == \"Ok\") {\r\n                s++;\r\n            }\r\n        }\r\n        return s / this.dlugosc;\r\n    };\r\n    Statek.prototype.trafienie = function (gdzie) {\r\n        this.statek[gdzie] = \"NieOk\";\r\n    };\r\n    __decorate([\r\n        trafiono()\r\n    ], Statek.prototype, \"trafienie\", null);\r\n    return Statek;\r\n}());\r\nexports.Statek = Statek;\r\nfunction trafiono() {\r\n    return function (target, name, descriptor) {\r\n        var originalMethod = descriptor.value;\r\n        descriptor.value = function () {\r\n            var args = [];\r\n            for (var _i = 0; _i < arguments.length; _i++) {\r\n                args[_i] = arguments[_i];\r\n            }\r\n            var result = originalMethod.apply(this, args);\r\n            alert(\"trafiono STATEK\");\r\n            return result;\r\n        };\r\n    };\r\n}\r\n\n\n//# sourceURL=webpack:///./src/statek.js?");

/***/ })

/******/ });